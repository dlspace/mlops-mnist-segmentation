# mlops-mnist-segmentation

Этот репозиторий про машинное обучение на данных набора MNIST, но адаптированных для задачи сегментации.

## Install python version and set venv

## Build docker image

```sh
make build_Dockerfile
```

## Install python with version

```sh
make python PYVER=3.11.6
```

## Build and setup virtual environment

```sh
make venv PYVER=3.11.6
```

## Run training

```sh
run-training
```

## Run prediction

```sh
run-prediction
```

## Print info about torch and cuda installed

```sh
print-cuda-info
```
