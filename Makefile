.PHONY: *

VENV=.venv
PYVER=3.11.9
DEVICE=gpu
DATASET_DIR := data
DOCKER_DIR := docker

build_Dockerfile:
	@echo "-----build_Dockerfile-----"
	cd docker && \
	sh ./docker-build.sh
	@echo "-----done-----"

python:
	@echo "-----install_python-----"
	pyenv install $(PYVER)
	pyenv rehash
	pyenv local $(PYVER)
	pyenv shell $(PYVER)
	@echo "-----done-----"

venv:
	@echo "-----venv-----"
	poetry env use $(PYVER)
	poetry env info
	poetry install --with dev --sync
	@echo
	@echo "Virtual environment has been created."
	@echo "Path to Python executable:"
	@echo `poetry env info -p`/bin/python
	@echo "-----done-----"

install_pre-commit: venv
	@echo "-----install_pre-commit-----"
	poetry run pre-commit install
	@echo "-----done-----"

tests:
	@echo "-----tests-----"
	poetry run pytest tests/
	@echo "-----done-----"

# train: venv
# 	@echo "-----run_training-----"
# 	poetry run run-training
# 	@echo "-----done-----"

# predict: venv
# 	@echo "-----run_training-----"
# 	poetry run run-prediction
# 	@echo "-----done-----"
