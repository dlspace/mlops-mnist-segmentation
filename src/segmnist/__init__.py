from importlib.metadata import version

from segmnist.logger import init_logger

__version__ = version("segmnist")


init_logger()
